# pinecone-recommendations

> The `pinecone-recommendations` repository is a RESTful API to demonstrate 
> interaction with the [Pinecone's](https://www.pinecone.io/) Similarity Search as 
> a Service. The resulting API is written using the [Spring Boot](<https://spring.io/projects/spring-boot>) 
> framework and [Java](<https://en.wikipedia.org/wiki/Java_(programming_language)>) version 11.

## Publications

This repository is related to an article published on DZone.com:

* TBD

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939

## Getting Started

In order to get started with the Similarity Search as a Service from Pinecone, an API key must be created using 
the following URL:

[Get Your API Key](https://www.pinecone.io/start/)

Once created, two additional attributes will be required:

* service name - a unique name for your service
* service authority - host and port to the Pinecone service handling your requests

## Configuration

The following properties must be set in order to utilize the `pinecone-recommendations`:

* pinecone.api-key: `{$API_KEY}`
* pinecone.service-authority: `{$SERVICE_AUTHORITY}`
* pinecone.service-name: `{$SERVICE_NAME}`
* server.port: `{$PORT}`

## Using the Pinecone Java client

In order to use the Pinecone Java client, the `./binaries/pinecone-java-client-dist-0.1.2.zip` needs to be expanded, 
then the following command needs to be executed in order to add the client to the local Maven repository:

```shell
mvn install:install-file \
 -Dfile=pinecone-java-client-0.1.2.jar \
 -Dsources=pinecone-java-client-0.1.2-sources.jar \
 -Djavadoc=pinecone-java-client-0.1.2-javadoc.jar \
 -DpomFile=pom-default.xml
```

This command will allow the following Maven dependency to be recognized:

```xml
<dependency>
    <groupId>io.pinecone</groupId>
    <artifactId>pinecone-client</artifactId>
    <version>0.1.2</version>
</dependency>
```

Currently, the following dependency is also required:

```xml
<dependency>
    <groupId>io.netty</groupId>
    <artifactId>netty-tcnative-boringssl-static</artifactId>
    <version>2.0.39.Final</version>
</dependency>
```

## Connectivity to Pinecone

Connectivity to the Pinecone service is achieved via the `PineconeConfiguration` class.  This class contains two helper 
methods (exposed using the `@Bean` annotation) to centralize client and connections to the Similarity Search as a Service:

```java
@Bean 
public PineconeClient pineconeClient() {
    return new PineconeClient(new PineconeClientConfig().withApiKey(pineconeConfigurationProperties.getApiKey()));
}

@Bean
public PineconeConnection pineconeConnection() {
    PineconeClient pineconeClient = new PineconeClient(new PineconeClientConfig().withApiKey(pineconeConfigurationProperties.getApiKey()));

    PineconeConnectionConfig pineconeConnectionConfig = new PineconeConnectionConfig()
            .withServiceAuthority(pineconeConfigurationProperties.getServiceAuthority())
            .withServiceName(pineconeConfigurationProperties.getServiceName());

    return pineconeClient.connect(pineconeConnectionConfig);
}
```

With the two helper beans in place, the Pinecone elements can be injected into any service or component as shown below:

```java
@RequiredArgsConstructor
@Service
public class RecommendationsService {
    private final PineconeClient pineconeClient;
    private final PineconeConnection pineconeConnection;

    // TODO - continue here 
}    
```

## Using `pinecone-recommendations`

In order to use the `pinecone-recommendations` service, the following URIs exist:

`// TODO - add URIs here once finalized`

## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.