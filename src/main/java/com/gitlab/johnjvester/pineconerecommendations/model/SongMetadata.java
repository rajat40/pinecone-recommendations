package com.gitlab.johnjvester.pineconerecommendations.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SongMetadata {
    private String name;
    private String genre;
}
