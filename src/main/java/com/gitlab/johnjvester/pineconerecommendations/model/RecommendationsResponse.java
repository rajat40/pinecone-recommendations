package com.gitlab.johnjvester.pineconerecommendations.model;

import lombok.Data;

import java.util.List;

@Data
public class RecommendationsResponse {
    private List<List<SongMetadata>> songs;
}
