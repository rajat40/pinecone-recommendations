package com.gitlab.johnjvester.pineconerecommendations.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration("pineconeConfigurationProperties")
@ConfigurationProperties(prefix = "pinecone")
public class PineconeConfigurationProperties {
    private String apiKey;
    private String serviceAuthority;
    private String serviceName;
}
