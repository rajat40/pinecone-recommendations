package com.gitlab.johnjvester.pineconerecommendations.services;

import com.gitlab.johnjvester.pineconerecommendations.model.SongMetadata;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class EmbeddingsService {

    public float[] toEmbedding(SongMetadata input) {
        return new float[]{0.1F,0.2F,0.3F};
    }

    public SongMetadata fromEmbedding(float[] data) {
        return SongMetadata.builder()
                .name("Never Gonna Give You Up")
                .genre("Pop")
                .build();
    }

    public SongMetadata fromId(String id) {
        return SongMetadata.builder()
                .name("Never Gonna Give You Up")
                .genre("Pop")
                .build();
    }
}
