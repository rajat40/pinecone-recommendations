package com.gitlab.johnjvester.pineconerecommendations.services;

import com.gitlab.johnjvester.pineconerecommendations.model.RecommendationsRequest;
import com.gitlab.johnjvester.pineconerecommendations.model.RecommendationsResponse;
import io.pinecone.PineconeClient;
import io.pinecone.PineconeConnection;
import io.pinecone.PineconeException;
import io.pinecone.QueryResponse;
import io.pinecone.UpsertResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class RecommendationsService implements InitializingBean {
    private final PineconeClient pineconeClient;
    private final PineconeConnection pineconeConnection;

    private final EmbeddingsService embeddingsService;

    public RecommendationsResponse makeRecommendation(RecommendationsRequest request) {
        log.debug("makeRecommendation()");
        log.debug("pineconeClient={}", pineconeClient.toString());
        log.debug("pineconeConnection={}", pineconeConnection.toString());

        // embed input
        float[][] queryData = new float[request.getSongs().size()][];
        queryData = request.getSongs().stream()
                .map(embeddingsService::toEmbedding)
                .collect(Collectors.toList())
                .toArray(queryData);

        // query Pinecone (optional: add retry with exponential backoff)
        QueryResponse queryResponse;

        try {
            queryResponse = pineconeConnection.send(pineconeClient.queryRequest()
                    .topK(5)
                    // .namespace("Rock")  // can use namespaces feature to limit queries to a particular genre (if vectors are upserted with genre as namespace)
                    .data(queryData));
            log.debug("queryResponse={}", queryResponse);
        } catch(PineconeException e) {
            log.error("Error querying Pinecone index:", e);
            throw e;
        }

        // convert vector ids to song metadata for response
        RecommendationsResponse response = new RecommendationsResponse();
        response.setSongs(queryResponse.getQueryResults().stream()
                .map((QueryResponse.SingleQueryResults singleQueryResults) -> {
                    return singleQueryResults.getIds().stream()
                            .map(embeddingsService::fromId)
                            .collect(Collectors.toList());
                })
                .collect(Collectors.toList())
        );

        return response;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        // upsert some data
        for (int i=0; i<1; i++) { // this could loop over e.g. lines in an input file
            try {
                UpsertResponse upsertResponse = pineconeConnection.send(
                        pineconeClient.upsertRequest()
                                .ids(Arrays.asList("v1", "v2"))
                                .data(new float[][]{
                                        {0.3F, 0.4F, 0.5F},
                                        {0.6F, 0.7F, 0.8F}
                                }));
                log.debug("upsertResponse={}", upsertResponse);
            } catch (PineconeException e) {
                log.error("Error upserting vectors into Pinecone index:", e);
            }
        }
    }
}
