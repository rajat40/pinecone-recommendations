package com.gitlab.johnjvester.pineconerecommendations.controllers;

import com.gitlab.johnjvester.pineconerecommendations.model.RecommendationsRequest;
import com.gitlab.johnjvester.pineconerecommendations.model.RecommendationsResponse;
import com.gitlab.johnjvester.pineconerecommendations.model.SongMetadata;
import com.gitlab.johnjvester.pineconerecommendations.services.RecommendationsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RequiredArgsConstructor
@Slf4j
@CrossOrigin
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class RecommendationsController {
    private final RecommendationsService recommendationsService;

    @GetMapping(value = "/recommend")
    public ResponseEntity<RecommendationsResponse> makeRecommendation() {
        log.debug("makeRecommendation()");

        // TODO: get inputs from request
        RecommendationsRequest recommendationsRequest = new RecommendationsRequest();
        recommendationsRequest.setSongs(Arrays.asList(
                SongMetadata.builder().name("Bohemian Rhapsody").genre("Rock").build(),
                SongMetadata.builder().name("Message In A Bottle").genre("Pop").build()
        ));

        return new ResponseEntity<>(
                recommendationsService.makeRecommendation(recommendationsRequest),
                HttpStatus.OK);
    }
}
